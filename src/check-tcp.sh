#!/bin/bash

HOST=0.0.0.0
PORT=$1

(echo health > /dev/tcp/$HOST/$PORT) >/dev/null 2>&1
if [[ $? -ne 0 ]]; then
    echo "UNHEATHLY - $HOST:$PORT is not open"
    exit 1
fi


