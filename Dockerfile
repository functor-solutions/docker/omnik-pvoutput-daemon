
FROM    python:3
COPY    src/    /opt/app
WORKDIR /data/app

#RUN     adduser --disabled-password --uid 10000 omnik
RUN     groupadd -g 10000 omnik && \
        useradd -r -u 10000 -g 10000 omnik

USER    omnik

HEALTHCHECK CMD /opt/app/check-tcp.sh 9500
CMD     ["python", "/opt/app/omnik-pvoutput-daemon.py"]

